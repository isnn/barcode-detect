#!/usr/bin/env python3

import time

import cv2

import json

from pyzbar import pyzbar
from datetime import datetime

def scan_now():

    vs = cv2.VideoCapture(0)
    time.sleep(0.2)
    
    outDir_ = "./out/"
    filename = "barLog.csv"
    found = set()
 
    fileData = open(outDir_ + filename, 'w')
    fileData.write("Waktu, Type, Data\n")

    # mengambil nilai dari json untuk seting  menampilkan frame
    with open("./cfg/conf.json") as f:
        config = json.load(f)

    if config["display"] :
        print("display started ... ")
    else :
        print("display closed")

    #mulai loop untuk deteksi dan decode barcode
    while True :
        ret, frame = vs.read()
        # frame = cv2.resize(frame, 480)

        barcodes = pyzbar.decode(frame)
        
        # menulis kolom untuk csv
        waktu = datetime.now()
        waktu = waktu.strftime("%H:%M:%S")
        
        # print(waktu)
        
        #deteksi barcode
        for barcode in barcodes :
            #membuat kotak tepi
            (x, y, w, h) = barcode.rect
            overlay = frame.copy()
            color_rect = (255, 46,139)
            cv2.rectangle(frame, (x, y), (x + w, y + h), color_rect, -1)
            
            #memberikan efek transparant  
            alpha = float(0.7)
            cv2.addWeighted(overlay, alpha, frame, 1-alpha, 0, frame)
            # print(barcode.rect)   #untuk menampilakn ukuran barcode

            #decode gambar ke data string
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type

            #membuat keterangan text di frame
            text = "{}{}".format(barcodeData, barcodeType)
            font = cv2.FONT_HERSHEY_SIMPLEX
            color = (46, 250, 255) 
            cv2.putText(frame,text,(x, y - 10), font, 0.7, color, 2)

            if barcodeData not in found:
                fileData.write("{},{},{}\n".format(waktu,barcodeData, barcodeType))
                fileData.flush()
                found.add(barcodeData)
        
        #menampilkan frame ke layar
        if config["display"]:
            cv2.imshow("latihan barcode scan", frame )
        
            #gunakan key untuk debugging dan exit
            key = cv2.waitKey(1) & 0xFF

            #ketika ditekan 'q' maka akan keluar
            if key == ord('q'):
                break

    cv2.destroyAllWindows()
    vs.release()

if __name__ == "__main__":
    print("oke")
    scan_now()